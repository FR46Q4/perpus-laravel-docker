#!/bin/bash
composer install
composer update
php artisan migrate:refresh --seed
php artisan db:seed
php artisan cache:clear